#!/usr/bin/python
import psycopg2
from config import config

try:
    conn = psycopg2.connect("dbname='tanya_dokter' user='postgres' host='localhost' password='a3b2c1d4'")
except:
    print("I am unable to connect to the database")

def update_stopwords(no, stopwords):
    """ update vendor name based on the vendor id """
    sql = """ UPDATE tanya_dokter
        SET remove_stopword = %s
        WHERE no = %s"""
    conn = None
    updated_rows = 0
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute the UPDATE  statement
        cur.execute(sql, (stopwords, no))
        # get the number of updated rows
        updated_rows = cur.rowcount
        # Commit the changes to the database
        conn.commit()
        # Close communication with the PostgreSQL database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return updated_rows

# import StopWordRemoverFactory class
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
 
factory = StopWordRemoverFactory()
stopword = factory.create_stop_word_remover()

cur = conn.cursor()
cur.execute("""SELECT no, raw_data from tanya_dokter""")
rows = cur.fetchall()
for row in rows:
    update_stopwords(row[0], stopword.remove(row[1]))

# Kalimat
# kalimat = 'Dengan Menggunakan Python dan Library Sastrawi saya dapat melakukan proses Stopword Removal'
# stop = stopword.remove(kalimat)
# print(stop)
