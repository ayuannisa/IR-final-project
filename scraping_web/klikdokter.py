import scrapy

class KlikDokter(scrapy.Spider):
    name = "klik"
    handle_httpstatus_list = [404]
    start_urls = [
        'http://www.klikdokter.com/indeks/penyakit'
    ]          

    def parse(self, response):
        #parsing url berdasarkan index alfabet
        for index in response.xpath('//div[@class="container-main"]/div[@id="main"]/div[@class="container-article"]/header[@class="topics-index--header"]/ul[@class="topics-index--header__list"]/li/a/text()').extract():
            if index != "All":
                index = response.urljoin("/indeks/penyakit/"+index)
                yield scrapy.Request(index, callback=self.parse)
        
        #parsing url berdasarkan nama penyakit yang ada di index alfabet        
        for disease in response.xpath('//div[@class="container-main"]/div[@id="main"]/div[@class="topics-index"]/div[@class="topics-index__container"]/div[@class="topics-index--section"]/ul[@class="topics-index--section__list"]/li/a/@href').extract():
            yield scrapy.Request(disease, callback=self.parse)

        #parsing semua data tentang penyakit        
        raw_data = response.xpath('//*[@id="main"]/div[2]/div/div/div[2]/div/h2/following-sibling::p/text()|//*[@id="main"]/div[2]/div/div/div[2]/div/h2/following-sibling::ul/li/text()|//*[@id="main"]/div[2]/div/div/div[2]/div/h2/following-sibling::ul/li/ul/li/text()').extract()
        # gejala = response.xpath('//div[@class = "container-main"]/div[@id = "main"]/div[@class = "container-article"]/div[@class = "topics--layout"]/div[@class = "topics--layout__wrapper clearfix"]/div[@class = "topics--layout-right topics--layout-right_triple-mode-center"]/div[@class = "tags--content"]/h2[@class="topics--content__section-header"][.="Gejala"]/following-sibling::p/text()[count(.|//div[@class = "container-main"]/div[@id = "main"]/div[@class = "container-article"]/div[@class = "topics--layout"]/div[@class = "topics--layout__wrapper clearfix"]/div[@class = "topics--layout-right topics--layout-right_triple-mode-center"]/div[@class = "tags--content"]/h2[@class="topics--content__section-header"][.="Pengobatan"]/preceding-sibling::p/text())=count(//div[@class = "container-main"]/div[@id = "main"]/div[@class = "container-article"]/div[@class = "topics--layout"]/div[@class = "topics--layout__wrapper clearfix"]/div[@class = "topics--layout-right topics--layout-right_triple-mode-center"]/div[@class = "tags--content"]/h2[@class="topics--content__section-header"][.="Pengobatan"]/preceding-sibling::p/text())]').extract()
        penyakit = response.xpath('//div[@class="tags--header"]/div[@class="tags--header__wrapper"]/aside[@class="tags--header__detail"]/div[@class="tags--header__detail-top"]/h1/text()').extract()
        yield {
            'disease': (penyakit),
            'raw_data' : (raw_data)
            # 'text': (gejala),
        }  

        

