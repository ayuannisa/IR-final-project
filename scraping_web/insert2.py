import scrapy
import psycopg2
import config
from scrapy.loader.processors import Join, MapCompose, TakeFirst

class PenyakitItem(scrapy.Item):
    nama_penyakit = scrapy.Field()
    data_penyakit = scrapy.Field()

class PenyakitLoader(scrapy.loader.ItemLoader):
    default_output_processor = TakeFirst()

    nama_penyakit_in = MapCompose(str.strip)
    nama_penyakit_out = Join()

    data_penyakit_in = MapCompose(str.strip)
    data_penyakit_out = Join()

class KlikDokter(scrapy.Spider):
    name = "klik"
    handle_httpstatus_list = [404]
    start_urls = [
        'http://www.klikdokter.com/indeks/penyakit/a' #,'http://www.klikdokter.com/penyakit/demam'
    ]

    def __init__(self):
        self.connection = psycopg2.connect(host='localhost', database='tanya_dokter', user='postgres', password='1234')
   

    
    def parse(self, response):
        def insert_data(no, nama, data):
            sql = """INSERT INTO penyakit(no, nama, raw_data)
                     VALUES(%s, %s, %s );"""
            conn = None
            #vendor_id = None
            try:
                # read database configuration
                params = config()
                # connect to the PostgreSQL database
                conn = psycopg2.connect(**params)
                # create a new cursor
                cur = conn.cursor()
                # execute the INSERT statement
                cur.execute(sql, (no, nama, data))
                # get the generated id back
                #vendor_id = cur.fetchone()[0]
                # commit the changes to the database
                conn.commit()
                # close communication with the database
                cur.close()
            except (Exception, psycopg2.DatabaseError) as error:
                print(error)
            finally:
                if conn is not None:
                    conn.close()

        item = PenyakitItem()
        i = 0
        #parsing url berdasarkan index alfabet
        for index in response.xpath('//div[@class="container-main"]/div[@id="main"]/div[@class="container-article"]/header[@class="topics-index--header"]/ul[@class="topics-index--header__list"]/li/a/text()').extract():
            if index != "All":
                index = response.urljoin("/indeks/penyakit/"+index)
                yield scrapy.Request(index, callback=self.parse)
                print(index)
        
        #parsing url berdasarkan nama penyakit yang ada di index alfabet        
        for disease in response.xpath('//div[@class="container-main"]/div[@id="main"]/div[@class="topics-index"]/div[@class="topics-index__container"]/div[@class="topics-index--section"]/ul[@class="topics-index--section__list"]/li/a/@href').extract():
            yield scrapy.Request(disease, callback=self.parse)
            print(disease)
            
        item['nama_penyakit'] = response.xpath('//div[@class="tags--header"]/div[@class="tags--header__wrapper"]/aside[@class="tags--header__detail"]/div[@class="tags--header__detail-top"]/h1/text()').extract()
        item['data_penyakit'] = response.xpath('//*[@id="main"]/div[2]/div/div/div[2]/div/h2/following-sibling::p/text()|//*[@id="main"]/div[2]/div/div/div[2]/div/h2/following-sibling::ul/li/text()|//*[@id="main"]/div[2]/div/div/div[2]/div/h2/following-sibling::ul/li/ul/li/text()').extract()
                #yield item
        i += 1
        insert_data(i, item['nama_penyakit'], item['data_penyakit'])
        print(item.get('nama_penyakit'))       

        
        # query = ("""INSERT INTO penyakit (id, nama_penyakit, raw_data, stopwords_data, stemming_data) VALUES(%s, %s, %s, %s, %s)""", (i, item.get('nama_penyakit'), item.get('data_penyakit'), "" , ""))
        # print(query)
       

        #i = id
        #self.connection.close()
        

        


    
        # yield {
        #     'disease': (penyakit),
        #     'raw_data' : (raw_data)
        #     # 'text': (gejala),
        # }  

        

