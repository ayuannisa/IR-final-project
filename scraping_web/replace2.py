#!/usr/bin/python
import psycopg2
import re
from config import config

try:
    conn = psycopg2.connect("dbname='tanya-dokter' user='postgres' host='localhost' password='1234'")
except:
    print("I am unable to connect to the database")

def replace_data_penyakit(raw_data, nama_penyakit, penyakit_id):
    sql = """UPDATE penyakit2
                SET raw_data = lower(%s),
                nama_penyakit = lower(%s)
                WHERE penyakit_id = %s"""
    conn = None
    updated_rows = 0
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute the UPDATE  statement
        cur.execute(sql, (raw_data, nama_penyakit, penyakit_id))
        # get the number of updated rows
        updated_rows = cur.rowcount
        # Commit the changes to the database
        conn.commit()
        # Close communication with the PostgreSQL database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
 
    return updated_rows


cur = conn.cursor()
regex = re.compile('[^A-Za-z0-9 ]')
cur.execute("""SELECT penyakit_id, nama_penyakit, raw_data from penyakit2""")
rows = cur.fetchall()
for row in rows:
    replace_data_penyakit(regex.sub('', row[2]), regex.sub('', row[1]), row[0])   

def update_stopwords(no, stopwords):
    """ update vendor name based on the vendor id """
    sql = """ UPDATE penyakit2
        SET remove_stopword = %s
        WHERE penyakit_id = %s"""
    conn = None
    updated_rows = 0
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute the UPDATE  statement
        cur.execute(sql, (stopwords, no))
        # get the number of updated rows
        updated_rows = cur.rowcount
        # Commit the changes to the database
        conn.commit()
        # Close communication with the PostgreSQL database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return updated_rows

# import StopWordRemoverFactory class
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
factory = StopWordRemoverFactory()
stopword = factory.create_stop_word_remover()
cur = conn.cursor()
cur.execute("""SELECT penyakit_id, raw_data from penyakit2""")
rows = cur.fetchall()
for row in rows:
    update_stopwords(row[0], stopword.remove(row[1]))

def update_stem(no, stem):
    """ update vendor name based on the vendor id """
    sql = """ UPDATE penyakit2
                SET stemming = %s
                WHERE penyakit_id = %s"""
    conn = None
    updated_rows = 0
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute the UPDATE  statement
        cur.execute(sql, (stem, no))
        # get the number of updated rows
        updated_rows = cur.rowcount
        # Commit the changes to the database
        conn.commit()
        # Close communication with the PostgreSQL database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
 
    return updated_rows

from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
factory = StemmerFactory()
stemmer = factory.create_stemmer()
cur = conn.cursor()
cur.execute("""SELECT penyakit_id, raw_data from penyakit2""")
rows = cur.fetchall()
for row in rows:
    update_stem(row[0], stemmer.stem(row[1]))