import scrapy
import psycopg2
import re
from config import config
from scrapy.loader.processors import Join, MapCompose, TakeFirst

class PenyakitItem(scrapy.Item):
    nama_penyakit = scrapy.Field()
    data_penyakit = scrapy.Field()

class PenyakitLoader(scrapy.loader.ItemLoader):
    default_output_processor = TakeFirst()

    nama_penyakit_in = MapCompose(str.strip)
    nama_penyakit_out = Join()

    data_penyakit_in = MapCompose(str.strip)
    data_penyakit_out = Join()

# def __init__(self):
#     self.connection = psycopg2.connect(host='localhost', database='tanya-dokter', user='postgres')

def insert_data(nama_penyakit, raw_data):
    try:
        conn = psycopg2.connect("dbname='tanya-dokter' user='postgres' host='localhost' password='1234'")
    except:
        print("I am unable to connect to the database")

    sql = """INSERT INTO penyakit2(nama_penyakit, raw_data)
             VALUES(%s,%s) RETURNING penyakit_id"""
    conn = None
    penyakit_id = None
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute the INSERT statement
        cur.execute(sql, (nama_penyakit, raw_data))
        # get the generated id back
        penyakit_id = cur.fetchone()[0]
        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return penyakit_id

class KlikDokter(scrapy.Spider):
    name = "klik"
    handle_httpstatus_list = [404]
    start_urls = [
        'http://www.klikdokter.com/indeks/penyakit/a'
    ]          

    def parse(self, response):
        item = PenyakitItem()

        #parsing url berdasarkan index alfabet
        for index in response.xpath('//*[@id="main"]/div[2]/header/ul/li/a/text()').extract():
            if index != "All":
                index = response.urljoin("/indeks/penyakit/"+index)
                yield scrapy.Request(index, callback=self.parse)
                print(index)
                
        #parsing url berdasarkan nama penyakit yang ada di index alfabet        
        for disease in response.xpath('//*[@id="main"]/div/div/div/ul/li/a/@href').extract():
            yield scrapy.Request(disease, callback=self.parse)
            
        item['nama_penyakit'] = response.xpath('//div[@class="tags--header"]/div[@class="tags--header__wrapper"]/aside[@class="tags--header__detail"]/div[@class="tags--header__detail-top"]/h1/text()').extract() 
        item['data_penyakit'] = response.xpath(
        		'//*[@id="main"]/div[2]/div/div/div[2]/div/h2/following-sibling::p/text()|'
        		'//*[@id="main"]/div[2]/div/div/div[2]/div/h2/following-sibling::ul/li/text()|'
        		'//*[@id="main"]/div[2]/div/div/div[2]/div/h2/following-sibling::ul/li/ul/li/text()|'
        		'//*[@id="main"]/div[2]/div/div/div[2]/div/ol/li/text()').extract()
        
        if len(item['nama_penyakit']) != 0:
        	insert_data(item['nama_penyakit'],item['data_penyakit'])
	        yield {
	        	'penyakit' : (item['nama_penyakit']),
	        	'data' : (item['data_penyakit'])
	        }