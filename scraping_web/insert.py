import scrapy
import psycopg2

class KlikDokter(scrapy.Spider):
    name = "klik"
    handle_httpstatus_list = [404]
    start_urls = [
        'http://www.klikdokter.com/indeks/penyakit'
    ]          

    def __init__(self):
        self.connection = psycopg2.connect(host='localhost', database='tanya-dokter', user='postgres')

    def parse(self, response):
        i = 0
        #parsing url berdasarkan index alfabet
        for index in response.xpath('//div[@class="container-main"]/div[@id="main"]/div[@class="container-article"]/header[@class="topics-index--header"]/ul[@class="topics-index--header__list"]/li/a/text()').extract():
            if index != "All":
                index = response.urljoin("/indeks/penyakit/"+index)
                yield scrapy.Request(index, callback=self.parse)
        
        #parsing url berdasarkan nama penyakit yang ada di index alfabet        
        for disease in response.xpath('//div[@class="container-main"]/div[@id="main"]/div[@class="topics-index"]/div[@class="topics-index__container"]/div[@class="topics-index--section"]/ul[@class="topics-index--section__list"]/li/a/@href').extract():
            yield scrapy.Request(disease, callback=self.parse)

        #parsing semua data tentang penyakit 
        i += 1 #id
        try:
            self.cursor = self.connection.cursor()
            raw_data = response.xpath('//*[@id="main"]/div[2]/div/div/div[2]/div/h2/following-sibling::p/text()|//*[@id="main"]/div[2]/div/div/div[2]/div/h2/following-sibling::ul/li/text()|//*[@id="main"]/div[2]/div/div/div[2]/div/h2/following-sibling::ul/li/ul/li/text()').extract()
            penyakit = response.xpath('//div[@class="tags--header"]/div[@class="tags--header__wrapper"]/aside[@class="tags--header__detail"]/div[@class="tags--header__detail-top"]/h1/text()').extract()
            self.cursor.execute("""INSERT INTO penyakit (id, nama_penyakit, raw_data, stopwords_data, stemming_data) VALUES(%s, %s, %s, %s, %s)""", (i, penyakit, raw_data, "" , ""))
            self.connection.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

    
        # yield {
        #     'disease': (penyakit),
        #     'raw_data' : (raw_data)
        #     # 'text': (gejala),
        # }  

        

