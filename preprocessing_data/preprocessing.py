import json

data = json.load(open('raw_data.json'))


result = []
for entry in data:
	if entry["disease"] and entry["raw_data"]:
		tempDict = {}
		tempDict["disease"] = entry["disease"][0]
		tempDict["raw_data"] = " ".join(entry["raw_data"])
		result.append(tempDict)
	
with open('clean_data.json', 'w') as outfile:
    json.dump(result, outfile)