#!/usr/bin/python
import psycopg2
from config import config

try:
    conn = psycopg2.connect("dbname='tanya_dokter' user='postgres' host='localhost' password='1234'")
except:
    print("I am unable to connect to the database")

def update_stem(no, stem):
    """ update vendor name based on the vendor id """
    sql = """ UPDATE penyakit
                SET stem = %s
                WHERE no = %s"""
    conn = None
    updated_rows = 0
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute the UPDATE  statement
        cur.execute(sql, (stem, no))
        # get the number of updated rows
        updated_rows = cur.rowcount
        # Commit the changes to the database
        conn.commit()
        # Close communication with the PostgreSQL database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
 
    return updated_rows

from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
factory = StemmerFactory()
stemmer = factory.create_stemmer()
cur = conn.cursor()
cur.execute("""SELECT no, raw_data from penyakit""")
rows = cur.fetchall()
for row in rows:
    update_stem(row[0], stemmer.stem(row[1]))






# stemming process
# sentence = 'Perekonomian Indonesia sedang dalam pertumbuhan yang membanggakan'
# output   = stemmer.stem(sentence)
# print(output)
# ekonomi indonesia sedang dalam tumbuh yang bangga
# print(stemmer.stem('Mereka meniru-nirukannya'))
# mereka tiru